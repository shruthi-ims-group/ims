import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { UserRoutingModule } from './user-routing.module';
import { ProfileComponent } from './profile/profile.component';
import { AttendanceComponent } from './attendance/attendance.component';
import { StdmarksComponent } from './stdmarks/stdmarks.component';
import { UserdashboardComponent } from './userdashboard/userdashboard.component';
import { SettingsComponent } from './settings/settings.component';


@NgModule({
  declarations: [ProfileComponent, AttendanceComponent, StdmarksComponent, UserdashboardComponent, SettingsComponent],
  imports: [
    CommonModule,
    UserRoutingModule,
    FormsModule
  ]
})
export class UserModule { }
