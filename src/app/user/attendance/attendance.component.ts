import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { StudentDataService } from '../student-data.service';
declare var $;
@Component({
  selector: 'app-attendance',
  templateUrl: './attendance.component.html',
  styleUrls: ['./attendance.component.css']
})
export class AttendanceComponent implements OnInit {

  constructor(private hc: HttpClient, private sd: StudentDataService) { }


  dummy: number = 2
  //stdAttendanceStatus: string[] = []

  //store all present status in array
  presentAttendance: string[] = []

  //store all absent status in array
  absentAttendance: string[] = []


  //conunt of present and absent
  totalAttenCount: number;

  presentCount: number = 30;

  absentCount: number = 10;





  ngOnInit() {
    //get batchname and stdid from service
    let batchData = this.sd.sendStdData()

    // //get student attendance
    this.hc.get(`/getStdAtten/${batchData["batchName"]}`).subscribe(res => {

      if (res["message"] == "attendance data not found") {
        console.log("attendance data not found")
      }
      else {
        res["message"].forEach(atten => {

          for (let x in atten.attendance) {

            if (atten.attendance[x].stdId == batchData["stdId"]) {

              if (atten.attendance[x].attendancestatus == "present") {

                this.presentAttendance.push(atten.attendance[x].attendancestatus)
              }
              if (atten.attendance[x].attendancestatus == "absent") {

                this.absentAttendance.push(atten.attendance[x].attendancestatus)
              }
            }
          }

        });
      }


      //find the length of presentAttendance count

      this.presentCount = this.presentAttendance.length


      //find the length of absentAttendance count
      this.absentCount = this.absentAttendance.length


      //sum of present count and absent count
      this.totalAttenCount = this.presentCount + this.absentCount
      // console.log("presentC", this.presentCount, "absentC", this.absentCount, "totalC", this.totalAttenCount)

      // console.log("svsdf", typeof (this.presentCount))
      // console.log("svs", typeof (this.absentCount))
      // console.log("sv", typeof (this.totalAttenCount))
      //js for progress bar
      $(document).ready(function ($) {
        function animateElements() {
          $('.progressbar').each(function () {
            var elementPos = $(this).offset().top;
            var topOfWindow = $(window).scrollTop();
            var percent = $(this).find('.circle').attr('data-percent');
            var animate = $(this).data('animate');
            if (elementPos < topOfWindow + $(window).height() - 30 && !animate) {
              $(this).data('animate', true);
              $(this).find('.circle').circleProgress({
                // startAngle: -Math.PI / 2,
                value: percent / 100,
                size: 160,
                thickness: 6,
                fill: {
                  color: '#663399'
                }
              }).on('circle-animation-progress', function (event, progress, stepValue) {
                $(this).find('strong').text((stepValue * 100).toFixed(0) + "%");
              }).stop();
            }
          });
        }

        animateElements();
        $(window).scroll(animateElements);
      });



    })
  }
}
