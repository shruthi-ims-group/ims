import { Component, OnInit } from '@angular/core';
import { StudentDataService } from '../student-data.service';
import { HttpClient } from '@angular/common/http';
declare var $;
@Component({
  selector: 'app-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.css']
})
export class SettingsComponent implements OnInit {

  constructor(private sds: StudentDataService, private hc: HttpClient) { }

  stdDataObj;

  //batchName
  //stdId
  changePass(changePassObj) {
    //update password
    this.hc.put(`/changeCredentials/${this.stdDataObj.stdId}`, changePassObj).subscribe(res => {


      alert(res["message"])
    })

    $("#myModal").modal("hide")

  }

  ngOnInit() {

    this.stdDataObj = this.sds.sendStdData()

  }
}
