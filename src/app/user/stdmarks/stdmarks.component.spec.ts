import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StdmarksComponent } from './stdmarks.component';

describe('StdmarksComponent', () => {
  let component: StdmarksComponent;
  let fixture: ComponentFixture<StdmarksComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StdmarksComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StdmarksComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
