import { Component, OnInit } from '@angular/core';
import { LoginService } from 'src/app/login.service';
import { HttpClient } from '@angular/common/http';
import { StudentDataService } from '../student-data.service';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {

  //student details array;
  studentDetails: any;

  constructor(private ls: LoginService, private hc: HttpClient, private sd: StudentDataService) { }

  ngOnInit() {
    //get student data based on student id

    let stdId = this.ls.sendStdUsername()

    //get student details based on student id
    this.hc.get(`/getStudentDetailsById/${stdId}`).subscribe(res => {

      if (res["message"] == "student data not found") {
        console.log("std data not found ")
      }
      else {
        this.studentDetails = res["message"]
        // console.log(this.studentDetails);

        this.sd.recieveStdData(this.studentDetails.bname, this.studentDetails.stdId)
      }
    })



  }

}
