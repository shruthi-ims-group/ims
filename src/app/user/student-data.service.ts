import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class StudentDataService {

  constructor() { }

  // student batchname and stdid
  stdData: object;
  //receive std batchname and stdId
  recieveStdData(batchname, stdid) {

    this.stdData = { batchName: batchname, stdId: stdid }

    // console.log("received data", batchname, stdid);
  }

  sendStdData() {
    return this.stdData
  }

}
