import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ProfileComponent } from './profile/profile.component';
import { StdmarksComponent } from './stdmarks/stdmarks.component';
import { AttendanceComponent } from './attendance/attendance.component';
import { UserdashboardComponent } from './userdashboard/userdashboard.component';
import { SettingsComponent } from './settings/settings.component';


const routes: Routes = [{
  path: "", component: UserdashboardComponent, children: [{ path: "profile", component: ProfileComponent },
  { path: "stdmarks", component: StdmarksComponent },
  { path: "sattendance", component: AttendanceComponent },
  { path: "settings", component: SettingsComponent },
  { path: '', redirectTo: "profile", pathMatch: "full" }
  ]
}];

// { path: "", redirectTo: "profile", pathMatch: "full" }
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UserRoutingModule { }
