import { Injectable } from '@angular/core';
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService implements HttpInterceptor {

  constructor() { }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

    const token = localStorage.getItem("token")

    //if token if found attach token with req object and forward 
    if (token) {
      //attach token to header of req object using bearer scheme
      const cloned = req.clone({ headers: req.headers.set("Authorization", "Bearer " + token) })

      return next.handle(cloned)
    }
    //if token is not found forward the same obj as it is
    else {
      return next.handle(req)
    }
  }
}
