import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { LoginService } from '../login.service';
import { ProtectGuard } from '../protect.guard';



@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  constructor(private ru: Router, private hc: HttpClient, private ls: LoginService, private pg: ProtectGuard) { }

  //login method
  loginData(loginObj) {
    //send credentials to server

    console.log(loginObj);

    this.ls.login(loginObj).subscribe(res => {
      if (res["message"] == "admin loggedin successfully") {

        let adminToken = res["admintoken"]

        //alert("admin loggedin successfully")

        localStorage.setItem("token", adminToken)

        this.ru.navigateByUrl("admindashboard")
      }
      if (res["message"] == "wrong password") {
        alert("wrong password")
      }

      if (res["message"] == "user loggedin successfully") {

        let userToken = res["usertoken"]

        //send username to login service 
        this.ls.receiveStdUsername(res["username"])

        //alert("user loggedin successfully")

        localStorage.setItem("token", userToken)

        this.ru.navigateByUrl("userdashboard")

      }
      if (res["message"] == "wrong credentials") {

        alert("wrong credentials")
      }

    })

  }


  // forgotPass() {
  //   this.ru.navigateByUrl("forgotpassword")
  // }
  ngOnInit() {

    this.pg.logOut()

  }
}
