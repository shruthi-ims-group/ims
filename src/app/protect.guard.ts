import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ProtectGuard implements CanActivate {

  //login status
  isLoggedIn: boolean = false;

  canActivate(): boolean {
    if (localStorage.getItem("token")) {
      this.isLoggedIn = true;
      return true;
    }
    else {
      this.isLoggedIn = false;
      return false
    }
  }
  //logout method
  logOut() {

    localStorage.clear()
    this.isLoggedIn = false
  }


}
