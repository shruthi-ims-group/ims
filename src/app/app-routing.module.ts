import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { AboutusComponent } from './aboutus/aboutus.component';
import { ContactusComponent } from './contactus/contactus.component';
import { LoginComponent } from './login/login.component';
import { ProtectGuard } from './protect.guard';
import { ForgotPasswordComponent } from './forgot-password/forgot-password.component';



const routes: Routes = [{ path: "home", component: HomeComponent },
{ path: "aboutus", component: AboutusComponent },
{ path: "contactus", component: ContactusComponent },
{ path: "login", component: LoginComponent },
{ path: "forgotpassword", component: ForgotPasswordComponent },
{ path: "admindashboard", loadChildren: './admin/admin.module#AdminModule', canActivate: [ProtectGuard] },
{ path: "userdashboard", loadChildren: './user/user.module#UserModule', canActivate: [ProtectGuard] },
{ path: '', redirectTo: "home", pathMatch: "full" },

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
