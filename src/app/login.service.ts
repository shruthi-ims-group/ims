import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  constructor(private hc: HttpClient) { }


  login(loginObj): Observable<any> {
    // console.log("loginObj", loginObj)
    //post credentials to server
    return this.hc.post("/login", loginObj)

  }



  //receive student username(stdId)...
  stdUsername: string;
  receiveStdUsername(username) {

    this.stdUsername = username
  }

  sendStdUsername() {
    return this.stdUsername;
  }


}
