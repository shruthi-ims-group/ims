import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { StudentdataService } from '../studentdata.service';
import { ProtectGuard } from 'src/app/protect.guard';
import { Router } from '@angular/router';
declare var $;
@Component({
  selector: 'app-studentdetails',
  templateUrl: './studentdetails.component.html',
  styleUrls: ['./studentdetails.component.css']
})
export class StudentdetailsComponent implements OnInit {


  stdDetails: any;
  batchname: string;
  // batchname: string = "batch_1"
  constructor(private hc: HttpClient, private sd: StudentdataService, private pg: ProtectGuard, private ru: Router) { }

  stdForm(formData) {

    console.log(formData)
    this.hc.post("/savestddata", { formData }).subscribe(res => {
      if (res["message"] == "student already existed") {
        alert("student already existed")
      }
      if (res["message"] == "std data not existed") {
        alert("std data not existed")
      }
      else {
        this.stdDetails = res["message"]
        // alert(res["mailstatus"])
        console.log("sdfs", this.stdDetails)
      }

      //response from checkauth middleware
      if (res["message"] == "no token found") {

        console.log("No token found")
      }
      if (res["message"] == "session expired") {
        alert("Session  Expired Please Login Again")

        //logout the admin when session is expired
        this.pg.logOut()

        //navigate user to login 
        this.ru.navigateByUrl("login")

      }
    })

    //hide modal whenn submit
    $("#myModal").modal("hide")
  }

  //global variables for edit form
  editname: string;
  editemail: string;
  editmobileno: any;
  editcousrse: string;
  editaddress: string;
  editgender: string;

  editbatchname: string;
  editstdid: string;

  //edit student details in table
  editStudentDatails(editObj) {

    this.editname = editObj.name
    this.editemail = editObj.email
    this.editmobileno = editObj.ph
    this.editcousrse = editObj.course
    this.editaddress = editObj.address
    this.editgender = editObj.gender

    this.editbatchname = editObj.bname
    this.editstdid = editObj.stdId

    console.log("eObj", editObj)
  }
  //delete student details in table
  deleteStudentDatails(deleteObj) {

    this.hc.delete(`/removeStudent/${deleteObj.stdId}/${deleteObj.bname}`).subscribe(res => {


      if (res["message"] == "data not found") {
        let dummyarr = []
        this.stdDetails = dummyarr

        console.log("data not found");

      }
      else {

        this.stdDetails = res["message"]
      }


      //response from checkauth middleware
      if (res["message"] == "no token found") {

        console.log("No token found")
      }
      if (res["message"] == "session expired") {
        alert("Session  Expired Please Login Again")

        //logout the admin when session is expired
        this.pg.logOut()
        //navigate user to login 
        this.ru.navigateByUrl("login")

      }

    })


    console.log("DObj", deleteObj);
  }
  //edit student details form
  editstdForm(editedForm) {


    this.hc.put(`/updatestdData/${this.editstdid}`, { batchname: this.batchname, editedForm }).subscribe(res => {
      if (res["message"] == "data not found") {

        console.log("data not found");

      }
      else {
        this.stdDetails = res["message"]
      }

      //response from checkauth middleware
      if (res["message"] == "no token found") {

        console.log("No token found")
      }
      if (res["message"] == "session expired") {
        alert("Session  Expired Please Login Again")

        //logout the admin when session is expired
        this.pg.logOut()
        //navigate user to login 
        this.ru.navigateByUrl("login")

      }
    })

    //hide modal
    $("#EditModal").modal("hide")
    console.log("editedstudent details ", editedForm)

  }

  ngOnInit() {

    this.batchname = this.sd.sendBatchName()


    this.hc.get(`/getstddata/${this.batchname}`).subscribe(res => {

      if (res["message"] == "std data not existed") {
        console.log("no student existed")
      }
      else {

        this.stdDetails = res["message"]
      }

      // response from checkauth middleware
      if (res["message"] == "no token found") {

        console.log("No token found")
      }
      if (res["message"] == "session expired") {
        alert("Session  Expired Please Login Again")

        //logout the admin when session is expired
        this.pg.logOut()

        //navigate user to login 
        this.ru.navigateByUrl("login")

      }
    })
  }

}
