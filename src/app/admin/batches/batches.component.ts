import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { StudentdataService } from '../studentdata.service';
import { ProtectGuard } from 'src/app/protect.guard';
declare var $
@Component({
  selector: 'app-batches',
  templateUrl: './batches.component.html',
  styleUrls: ['./batches.component.css']
})
export class BatchesComponent implements OnInit {

  constructor(private ru: Router, private hc: HttpClient, private sd: StudentdataService, private pg: ProtectGuard) { }



  //batchname for form
  batch: string;
  //get all batch data
  batchData: any;
  //batch length
  batchLength: any;
  //array for find batch length 
  newBatchLenth: object[] = []

  //assign to and from date
  todate: any;
  fromdate: any;

  //add a new batch in mongodb
  addNewBatch(value) {
    console.log("value ds", value);
    let fromDate = new Date(value.Fromdate)
    let toDate = new Date(value.Todate)


    // convert given date into timestamp
    let pickerfromDate = new Date(fromDate.getFullYear(), fromDate.getMonth(),
      fromDate.getDate(), 0o0, 0o0, 0o0).getTime();

    let pickerToDate = new Date(toDate.getFullYear(),
      toDate.getMonth(), toDate.getDate(), 23, 59, 59).getTime();


    value.Fromdate = pickerfromDate;
    value.Todate = pickerToDate;

    console.log("after timestamp", value)


    //add new batch request 
    this.hc.post("/addnewbatch", value).subscribe(res => {
      if (res["message"] == "batch data is empty") {
        console.log("batch data is empty")
      }
      else if (res["message"] == "Batchname is already Exists") {
        alert("Batch is already Exists")
      }
      else {
        this.batchData = res["message"]
      }

      //response from checkauth middleware
      if (res["message"] == "no token found") {

        console.log("No token found")
      }
      if (res["message"] == "session expired") {
        alert("Session  Expired Please Login Again")

        //logout the admin when session is expired
        this.pg.logOut()

        //navigate user to login 
        this.ru.navigateByUrl("login")

      }

    })
    //hide modal when submit
    $("#myModal").modal("hide")
  }



  //navigate to studentDetails from batches
  batchNav(batchname) {

    console.log(batchname);


    //send batchname to service
    this.sd.stdList(batchname);

    //navigate to studentDetails from batches    
    this.ru.navigate(['/admindashboard/studentdetails'])

  }

  //removing batch 
  removeBatch(batchname) {

    console.log("removing batch name", batchname);
    this.hc.delete(`/removeBatch/${batchname}`).subscribe(res => {
      if (res["message"] == "batch data not found") {

        console.log("batch data not found")
      }
      else {
        this.batchData = res["message"]
      }

      //response from checkauth middleware
      if (res["message"] == "no token found") {

        console.log("No token found")
      }
      if (res["message"] == "session expired") {
        alert("Session  Expired Please Login Again")

        //logout the admin when session is expired
        this.pg.logOut()

        //navigate user to login 
        this.ru.navigateByUrl("login")

      }

    })
  }
  //edit date duration of date in modal and pass curret duration to form
  batchnameedit: string;
  editDate(batchdate) {

    console.log("batch duration", batchdate)

    this.fromdate = batchdate.Fromdate
    this.todate = batchdate.Todate;

    console.log("todate", this.todate, "fromdate", this.fromdate)
    this.batchnameedit = batchdate.batchname

  }

  //edit batch duration in form
  editBatchDuration(durationObj) {


    let fromDate = new Date(durationObj.Fromdate)
    let toDate = new Date(durationObj.Todate)
    // convert given date into timestamp
    let pickerFromDate = new Date(fromDate.getFullYear(), fromDate.getMonth(),
      fromDate.getDate(), 0o0, 0o0, 0o0).getTime();

    let pickerToDate = new Date(toDate.getFullYear(),
      toDate.getMonth(), toDate.getDate(), 23, 59, 59).getTime();

    //assign timestamp to duration object
    durationObj.Fromdate = pickerFromDate
    durationObj.Todate = pickerToDate
    console.log("edited duration", durationObj)


    //update the batch duration
    this.hc.put("/updatebatchDuration", { batchname: this.batchnameedit, updatedduration: durationObj }).subscribe(res => {

      if (res["message"] == "no batchs found") {
        console.log("No batchs found")
      }
      else {
        this.batchData = res["message"]

      }
      //response from checkauth middleware
      if (res["message"] == "no token found") {

        console.log("No token found")
      }
      if (res["message"] == "session expired") {
        alert("Session  Expired Please Login Again")

        //logout the admin when session is expired
        this.pg.logOut()

        //navigate user to login 
        this.ru.navigateByUrl("login")

      }
    })
    //hide modal
    $("#editModal").modal("hide")
  }
  ngOnInit() {
    //get all batch info
    this.hc.get("/getallbatches").subscribe(res => {
      if (res["message"] == "data not found") {
        console.log("data not found")
      }
      else {

        this.batchData = res["message"]
        this.newBatchLenth = res["message"]
      }

      //response from checkauth middleware
      if (res["message"] == "no token found") {

        console.log("No token found")
      }
      if (res["message"] == "session expired") {
        alert("Session  Expired Please Login Again")

        //logout the admin when session is expired
        this.pg.logOut()

        //navigate user to login 
        this.ru.navigateByUrl("login")

      }

      console.log("batches", this.batchData);

      //increment the batch number
      this.batchLength = this.newBatchLenth.length

      this.batchLength++;

      this.batch = ("batch_" + this.batchLength)

      console.log("success", this.batch)
    })

  }
}
