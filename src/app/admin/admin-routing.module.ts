import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AdmindashboardComponent } from './admindashboard/admindashboard.component';
import { BatchesComponent } from './batches/batches.component';
import { AttendanceComponent } from './attendance/attendance.component';
import { MarksComponent } from './marks/marks.component';
import { StudentdetailsComponent } from './studentdetails/studentdetails.component';
import { ViewstudentattendanceComponent } from './viewstudentattendance/viewstudentattendance.component';


const routes: Routes = [{
  path: "", component: AdmindashboardComponent,
  children: [{ path: "batches", component: BatchesComponent },
  { path: "studentdetails", component: StudentdetailsComponent },
  { path: "attendance", component: AttendanceComponent },
  { path: "viewattendance", component: ViewstudentattendanceComponent },
  { path: "marks", component: MarksComponent },
  { path: '', redirectTo: 'batches', pathMatch: "full" }]
}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminRoutingModule { }
