import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ProtectGuard } from 'src/app/protect.guard';
import { Router } from '@angular/router';

@Component({
  selector: 'app-viewstudentattendance',
  templateUrl: './viewstudentattendance.component.html',
  styleUrls: ['./viewstudentattendance.component.css']
})
export class ViewstudentattendanceComponent implements OnInit {

  tableHide: boolean = false;
  batchData: any;
  allStudentsAtten: any;
  //today = new Date()
  constructor(private hc: HttpClient, private pg: ProtectGuard, private ru: Router) { }
  allStdAttenArr: object[] = []
  //search details of batches
  searchDeatils(sdetails) {

    //get all batches if batches is all
    // alert(sdetails.batch)
    // this.allStudentsAtten.splice(0, this.allStudentsAtten.length);
    console.log("before", this.allStudentsAtten)

    var checkDate = new Date(sdetails.checkdate);

    var pickerDate = new Date(checkDate.getFullYear(), checkDate.getMonth(),
      checkDate.getDate(), 0o0, 0o0, 0o0).getTime();



    if (sdetails.batch == "allbatches") {
      //get all batches attendance
      this.hc.get(`/getAttendance/${pickerDate}`).subscribe(res => {
        if (res["message"] == "attendance not found") {
          console.log("attendance not found")
        }
        else {
          // this.allStudentsAtten = res["message"]
          // console.log("all atten", res["message"])
          this.allStdAttenArr.length = 0
          res["message"].forEach(attenObj => {

            for (let x in attenObj.attendance) {

              this.allStdAttenArr.push(attenObj.attendance[x])
            }
          });
          // this.allStudentsAtten = this.allStdAttenArr

          this.tableHide = true
          console.log("after all batches", this.allStudentsAtten)
        }

        //response from checkauth middleware
        if (res["message"] == "no token found") {

          console.log("No token found")
        }
        if (res["message"] == "session expired") {
          alert("Session  Expired Please Login Again")

          //logout the admin when session is expired
          this.pg.logOut()

          //navigate user to login 
          this.ru.navigateByUrl("login")

        }
      })
    }
    else {
      //get student data by batchnames
      // let checkDate = new Date(sdetails.checkdate);

      // let pickerDate = new Date(checkDate.getFullYear(), checkDate.getMonth(),
      //   checkDate.getDate(), 0o0, 0o0, 0o0).getTime();


      this.hc.get(`/getStdAttendanceByBatchName/${sdetails.batch}/${pickerDate}`).subscribe(res => {

        if (res["message"] == "attendance data is not found") {

          console.log("attendance array not found")
        }
        else {
          // this.allStudentsAtten.splice(0, this.allStudentsAtten.length);
          this.tableHide = true
          this.allStdAttenArr.length = 0
          console.log("dsfdsf", res["message"])
          res["message"].forEach(attenObj => {

            for (let x in attenObj.attendance) {

              this.allStdAttenArr.push(attenObj.attendance[x])
            }
          });
          // this.allStudentsAtten = this.allStdAttenArr
          console.log("after pp batches", this.allStudentsAtten)
        }

        //response from checkauth middleware
        if (res["message"] == "no token found") {

          console.log("No token found")
        }
        if (res["message"] == "session expired") {
          alert("Session  Expired Please Login Again")

          //logout the admin when session is expired
          this.pg.logOut()

          //navigate user to login 
          this.ru.navigateByUrl("login")

        }

      })

    }
  }

  ngOnInit() {
    //get all batch info
    this.hc.get("/getallbatches").subscribe(res => {

      if (res["message"] == "data not found") {

        console.log("batch data not found")
      }
      else {

        this.batchData = res["message"]
      }

      //response from checkauth middleware
      if (res["message"] == "no token found") {

        console.log("No token found")
      }
      if (res["message"] == "session expired") {
        alert("Session  Expired Please Login Again")

        //logout the admin when session is expired
        this.pg.logOut()

        //navigate user to login 
        this.ru.navigateByUrl("login")

      }

    })

  }

}
