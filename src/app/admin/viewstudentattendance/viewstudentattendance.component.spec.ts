import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewstudentattendanceComponent } from './viewstudentattendance.component';

describe('ViewstudentattendanceComponent', () => {
  let component: ViewstudentattendanceComponent;
  let fixture: ComponentFixture<ViewstudentattendanceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewstudentattendanceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewstudentattendanceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
