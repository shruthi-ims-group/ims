import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ProtectGuard } from 'src/app/protect.guard';
import { Router } from '@angular/router';

@Component({
  selector: 'app-attendance',
  templateUrl: './attendance.component.html',
  styleUrls: ['./attendance.component.css']
})
export class AttendanceComponent implements OnInit {



  batchData: any;
  allStudents: any;
  studentsArray: object[] = []

  attendanceArray: object[] = []
  date = new Date()

  hideTable: boolean = false;


  selectbatch: boolean = true;

  notFoundStatus: boolean = false;
  // timeStamp = this.date.getTime()

  constructor(private hc: HttpClient, private pg: ProtectGuard, private ru: Router) { }

  //search details of batches
  searchDetails(sdetails) {
    //get student data by batchnames
    // alert(sdetails)
    this.hc.get(`/getStdByBatchName/${sdetails}`).subscribe(res => {

      if (res["message"] == "std not found") {

        this.hideTable = false;
        this.selectbatch = false;
        this.notFoundStatus = true;

        console.log("std not found")
      }
      else {
        this.studentsArray.length = 0
        console.log("allstudents array", this.allStudents)
        this.notFoundStatus = false;
        this.selectbatch = false;
        this.hideTable = true;

        this.allStudents = res["message"]
        this.studentsArray = res["message"]
      }
      //response from checkauth middleware
      if (res["message"] == "no token found") {

        console.log("No token found")
      }
      if (res["message"] == "session expired") {
        alert("Session  Expired Please Login Again")

        //logout the admin when session is expired
        this.pg.logOut()

        //navigate user to login 
        this.ru.navigateByUrl("login")

      }
    })

  }

  //batch name for sending attendance array
  batchname: string;
  timeStampp: number;
  //adding attendance for students
  attendance(val, sobj) {
    sobj.attendancestatus = val;

    let pickerDate = new Date(this.date.getFullYear(), this.date.getMonth(),
      this.date.getDate(), 0o0, 0o0, 0o0).getTime();

    sobj.timestamp = pickerDate;

    this.timeStampp = sobj.timestamp;

    this.batchname = sobj.bname
    if (this.attendanceArray.length == 0) {
      this.attendanceArray.push(sobj)
      console.log("after pushing1", this.attendanceArray)
    }
    else {

      for (var attendanceObj in this.attendanceArray) {

        var value = this.attendanceArray[attendanceObj]

        // console.log("value", value)
      }
      if (value["stdId"] !== sobj.stdId) {

        this.attendanceArray.push(sobj)
        console.log("after pushing2", this.attendanceArray)
      }
      else {

        this.attendanceArray.splice(+attendanceObj, 1, sobj)
        console.log("after splice", this.attendanceArray)
      }
      console.log(this.attendanceArray)
    }
  }


  //method for  remove duplicates from attendanceArray
  removeDuplicates(originalArray, prop) {
    var newArray = [];
    var lookupObject = {};

    for (var i in originalArray) {
      lookupObject[originalArray[i][prop]] = originalArray[i];
    }

    for (i in lookupObject) {
      newArray.push(lookupObject[i]);
    }
    return newArray;
  }


  //new modified attendanceArray
  newAttenArr: any;
  //sending attendancedata to database
  submitAttendance() {
    //alert("req sent")
    this.newAttenArr = this.removeDuplicates(this.attendanceArray, 'stdId');
    // console.log("before removing duplicates", this.attendanceArray)
    // console.log("after removing duplicates", this.attend)
    // console.log(attenObj)
    console.log("after submit", this.newAttenArr)

    //compare studentsArray and newAttenArr

    if (this.studentsArray.length == this.newAttenArr.length) {

      // console.log("batchname", this.batchname, "timestamp", this.timeStampp)
      this.hc.post(`/saveAttendance/${this.batchname}/${this.timeStampp}`, this.newAttenArr).subscribe(res => {

        this.attendanceArray.length = 0
        if (res["message"] == "attendance array is saved successfully") {
          alert("attendance  is saved successfully")
        }

        //response from checkauth middleware
        if (res["message"] == "no token found") {

          console.log("No token found")
        }
        if (res["message"] == "session expired") {
          alert("Session  Expired Please Login Again")

          //logout the admin when session is expired
          this.pg.logOut()

          //navigate user to login 
          this.ru.navigateByUrl("login")

        }
      })


    }
    else {

      alert("please select all attendance status")
    }
  }

  ngOnInit() {
    //get all batch info
    this.hc.get("/getallbatches").subscribe(res => {

      if (res["message"] == "data not found") {

        console.log("batch data not found")
      }
      else {

        this.batchData = res["message"]
      }

      //response from checkauth middleware
      if (res["message"] == "no token found") {

        console.log("No token found")
      }
      if (res["message"] == "session expired") {
        alert("Session  Expired Please Login Again")

        //logout the admin when session is expired
        this.pg.logOut()

        //navigate user to login 
        this.ru.navigateByUrl("login")

      }

    })
  }
}


