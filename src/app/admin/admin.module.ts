import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { AdminRoutingModule } from './admin-routing.module';
import { AdmindashboardComponent } from './admindashboard/admindashboard.component';
import { BatchesComponent } from './batches/batches.component';
import { AttendanceComponent } from './attendance/attendance.component';
import { MarksComponent } from './marks/marks.component';
import { StudentdetailsComponent } from './studentdetails/studentdetails.component';
import { ViewstudentattendanceComponent } from './viewstudentattendance/viewstudentattendance.component';
import { FlexLayoutModule } from '@angular/flex-layout';

@NgModule({
  declarations: [AdmindashboardComponent, BatchesComponent, AttendanceComponent, MarksComponent, StudentdetailsComponent, ViewstudentattendanceComponent],
  imports: [
    CommonModule,
    AdminRoutingModule,
    FormsModule,
    FlexLayoutModule
  ]
})
export class AdminModule { }
