//import express module
const exp = require("express")

const app = exp()


//import body-parser momdule
const bodyParser = require("body-parser")

app.use(bodyParser.json())

//import path module
const path = require("path")

//import jsonwebtoken
const jwt = require("jsonwebtoken")

//import bcrypt module

const bcrypt = require("bcrypt")

//import checkToken middleware

const checkAuth = require('./checkToken')

//place js bundles in server
app.use(exp.static(path.join(__dirname, './dist/IMS-APP')))

//import mongoDB driver
const mc = require("mongodb").MongoClient;

//mongodb url
const mongoUrl = "mongodb+srv://shruthi5:shruthi5@cluster0-d5mfw.gcp.mongodb.net/test?retryWrites=true&w=majority"

//collection names for Global access
var batchCollection;
var adminloginCollection;
var userloginCollection;
var studentCollection;
var attendanceCollection;
var otpCollection;

//conect to mongodb
mc.connect(mongoUrl, { useNewUrlParser: true, useUnifiedTopology: true }, (err, client) => {
    if (err) {

        console.log("error in connecting to mongoDb", err)

    }
    else {

        //mongodb database name

        var dbO = client.db("ims")

        //batch collection
        batchCollection = dbO.collection("batches")

        //adminlogin collection
        adminloginCollection = dbO.collection("admin")

        //userlogin collection
        userloginCollection = dbO.collection("user")
        //student collection
        studentCollection = dbO.collection("students")

        //otp collection
        otpCollection = dbO.collection("otp")

        //Attendance collection
        attendanceCollection = dbO.collection("Attendance")
        console.log("successfully connected to mongoDB");

    }

})
//Request Handlers
//Add new batch req handler

app.post("/addnewbatch", (req, res) => {
    console.log(req.body)

    batchCollection.findOne({ batchname: req.body.batchname }, (err, Oldbatch) => {
        if (err) {

            console.log("error infinding batches")
        }
        else if (Oldbatch == null) {

            batchCollection.insertOne(req.body, (err, result) => {

                if (err) {

                    console.log("error in insering new batch", err)
                }
                else {

                    // res.json({ message: "succesfully added new batch" })

                    batchCollection.find().toArray((err, bArray) => {

                        if (err) {
                            console.log("error in reading data", err)
                        }
                        else if (bArray.length == 0) {
                            res.json({ message: "batch data is empty" })
                        }
                        else {
                            res.json({ message: bArray })
                        }
                    })
                }
            })

        }
        else {

            res.json({ message: "Batchname is already Exists" })
        }

    })


})
//get all batches data

app.get("/getallbatches", (req, res) => {

    batchCollection.find().toArray((err, batchArray) => {

        if (err) {

            console.log("error in reading data", err);

        }
        else if (batchArray.length == 0) {

            res.json({ message: "data not found" })

        }
        else {

            res.json({ message: batchArray })
        }
    })
})

//get student data related to perticular batch name

app.get('/getstddata/:batchname', checkAuth, (req, res) => {

    console.log(req.params.batchname)

    studentCollection.find({ bname: req.params.batchname }).toArray((err, stdArray) => {


        if (err) {

            console.log("error in reading data", err)
        }
        else if (stdArray.length == 0) {

            res.json({ message: "std data not existed" })
        }
        else {
            res.json({ message: stdArray })
        }
    })
})


//save student details in students collection
app.post("/savestddata", (req, res) => {

    console.log(req.body)

    studentCollection.findOne({ stdId: req.body.formData.stdId }, (err, oldStd) => {

        if (err) {

            console.log("error in redading data", err)
        }
        else if (oldStd !== null) {

            res.json({ message: "student already existed" })

        }
        else {
            studentCollection.insertOne(req.body.formData, (err, result) => {
                if (err) {
                    console.log("error in inserting std details")
                }
                else {
                    //take name from req.body and bcrypt name and save it as password in db

                    // let stdName = req.body.formData.name

                    // console.log("stdname", stdName)

                    bcrypt.hash(req.body.formData.name, 8, (err, hashedPass) => {

                        if (err) {
                            console.log("error in hashing password")
                        }
                        else {


                            // console.log("after hash", req.body.formData)
                            userloginCollection.insertOne({ username: req.body.formData.stdId, password: hashedPass }, (err, result) => {

                                if (err) {

                                    console.log("error in saving data", err)
                                }
                                else {

                                    // //send mail to users when regiter
                                    // var mailOptions = {
                                    //     from: 'murariadurthi.5859@gmail.com',
                                    //     to: req.body.formData.email,
                                    //     subject: 'sending email using node.js',
                                    //     text: 'Thanks for joining in ims',
                                    //     html: `<div><h3>Welcome to Ims</h3> <p>Thanks for joining in ims</p> <p>Your login credentials are</p> <p>Username:${req.body.formData.stdId}</p> <p>Password:${req.body.formData.name}</p></div>`

                                    // }

                                    // tranporter.sendMail(mailOptions, (err, info) => {
                                    //     if (err) {

                                    //         console.log("error in sending mail", err)
                                    //     }
                                    //     else {

                                    //         console.log("email sent:" + info.response);


                                    //     }
                                    // })


                                    // res.json({ message: "successfully saved data" })
                                    studentCollection.find({ bname: req.body.formData.bname }).toArray((err, stdArray) => {

                                        if (err) {

                                            console.log("error in reading data", err)
                                        }
                                        else if (stdArray.length == 0) {
                                            res.json({ message: "std data not existed" })

                                        }
                                        else {
                                            res.json({ message: stdArray })
                                        }
                                    })

                                }
                            })
                        }

                    })
                }
            })
        }
    })
})

//removing batch 
app.delete("/removeBatch/:batchname", (req, res) => {

    batchCollection.deleteOne({ batchname: req.params.batchname }, (err, oldBatch) => {

        if (err) {
            console.log("error in deleting data", err)
        }
        else {
            studentCollection.deleteMany({ bname: req.params.batchname }, (err, oldStdObjs) => {

                if (err) {
                    console.log("error in deleting student details");

                }
                else {
                    batchCollection.find().toArray((err, bArray) => {
                        if (err) {
                            console.log("error in reading batch data")
                        }
                        else if (bArray.length == 0) {

                            res.json({ message: "batch data not found" })
                        }
                        else {
                            res.json({ message: bArray })
                        }
                    })
                }
            })

        }
    })
})

//update the batch duration req handler
app.put("/updatebatchDuration", (req, res) => {

    console.log("batchname", req.body.batchname)
    console.log("todate and fromdate", req.body.updatedduration);

    batchCollection.updateOne({ batchname: req.body.batchname },
        { $set: { Fromdate: req.body.updatedduration.Fromdate, Todate: req.body.updatedduration.Todate } }, (err, updatedObj) => {

            if (err) {

                console.log("error in updating data", err)
            }
            else {
                //get all batches data again 
                batchCollection.find().toArray((err, result) => {

                    if (err) {
                        console.log("error in finding data", err)
                    }
                    else if (result.length == 0) {
                        res.json({ message: "no batchs found" })
                    }
                    else {
                        res.json({ message: result })

                    }
                })

            }
        })
})

//update student details 

app.put('/updatestdData/:stdId', (req, res) => {

    studentCollection.updateOne({ stdId: req.params.stdId }, { $set: { name: req.body.editedForm.name, ph: req.body.editedForm.ph, course: req.body.editedForm.course, gender: req.body.editedForm.gender, address: req.body.editedForm.address } }, (err, result) => {

        if (err) {

            console.log("error in updating student data", err);

        }
        else {

            studentCollection.find({ bname: req.body.batchname }).toArray((err, stdArray) => {

                if (err) {
                    console.log("error in reading data ", err)

                }

                else if (stdArray.length == 0) {
                    res.json({ message: "data not found" })

                }
                else {

                    res.json({ message: stdArray })

                }
            })
        }
    })
})

//remove student in student collection

app.delete("/removeStudent/:stdId/:batchname", (req, res) => {

    studentCollection.deleteOne({ stdId: req.params.stdId }, (err, result) => {

        if (err) {
            console.log("error in deleteing student", err)
        }
        else {

            //delete userdetails in userlogin from collection
            userloginCollection.deleteOne({ username: req.params.stdId }, (err, deleteUser) => {

                if (err) {
                    console.log("error in deleting user", err);

                }
                else {
                    studentCollection.find({ bname: req.params.batchname }).toArray((err, stdArray) => {

                        if (err) {
                            console.log("error in reading data ", err)

                        }

                        else if (stdArray.length == 0) {

                            res.json({ message: "data not found" })

                        }
                        else {

                            res.json({ message: stdArray })

                        }
                    })
                }
            })


        }
    })
})

//reqests from attendance component
//get all student data from db
app.get("/getAllStudentData", (req, res) => {

    studentCollection.find().toArray((err, allStdArray) => {

        if (err) {
            console.log("error in reading data", err)
        }
        else if (allStdArray.length == 0) {
            res.json({ message: "students are not existed" })
        }
        else {

            res.json({ message: allStdArray })
        }

    })
})

//get students based on by batch names
app.get("/getStdByBatchName/:batchname", (req, res) => {
    console.log(req.params.batchname)
    studentCollection.find({ bname: req.params.batchname }).toArray((err, stdArray) => {

        if (err) {
            console.log("error in reading data", err)
        }
        else if (stdArray.length == 0) {

            res.json({ message: "std not found" })
        }
        else {
            res.json({ message: stdArray })
        }
    })
})

//save attendance into db
app.post("/saveAttendance/:batchname/:timestamp", (req, res) => {
    // console.log(req.body)

    // console.log(req.params.batchname, req.params.timestamp)

    attendanceCollection.insertOne({ attendance: req.body, batch: req.params.batchname, timestamp: req.params.timestamp }, (err, result) => {

        if (err) {

            console.log("error in saving attendance", err)
        }
        else {

            res.json({ message: "attendance array is saved successfully" })
        }
    })
})


//get all batches attendance
app.get("/getAttendance/:date", (req, res) => {

    attendanceCollection.find({ timestamp: req.params.date }).toArray((err, result) => {
        if (err) {
            console.log(err)
        }
        else if (result.length == 0) {

            res.json({ message: "attendance not found" })
        }
        else {
            //console.log(result)
            res.json({ message: result })
        }

    })
})

//get std attendance based on batch and date
app.get(`/getStdAttendanceByBatchName/:batchname/:date`, (req, res) => {

    console.log("batchname", req.params.batchname, "date", req.params.date)

    attendanceCollection.find({ batch: req.params.batchname, timestamp: req.params.date }).toArray((err, attObj) => {
        if (err) {

            console.log("error in reading data", err)
        }
        else if (attObj.length == 0) {
            console.log(attObj)
            res.json({ message: "attendance data is not found" })
        }
        else {
            console.log(attObj)
            res.json({ message: attObj })
        }
    })
})

//request handlers for login 
app.post("/login", (req, res) => {

    console.log(req.body)

    adminloginCollection.findOne({ username: req.body.username, password: req.body.password }, (err, adminloginResult) => {

        if (err) {
            console.log("error in checking admin credentials", err)
        }
        else if (adminloginResult !== null) {

            // res.json({ message: "admin loggedin successfully" })
            //genrating token for admin
            jwt.sign({ username: adminloginResult.username }, 'secret', { expiresIn: 1000 * 60 * 10 }, (err, signedToken) => {

                if (err) {
                    console.log("error in generating token", err)
                }
                else {
                    res.json({ message: "admin loggedin successfully", admintoken: signedToken })
                }
            })

        }
        else {
            // res.json({ message: "wrong credentials" })
            userloginCollection.findOne({ username: req.body.username },
                (err, userloginResult) => {
                    if (err) {
                        console.log("error in checking user credentials", err)
                    }
                    else if (userloginResult !== null) {
                        //res.json({ message: "user logged in successfully" })

                        //compare given password with saved password
                        bcrypt.compare(req.body.password, userloginResult.password, (err, result) => {
                            if (err) {
                                console.log("error in password comparing")
                            }
                            else if (result == false) {

                                res.json({ message: "wrong password" })
                            }
                            else {
                                //genrating token for user
                                jwt.sign({ username: userloginResult.username }, 'secret', { expiresIn: 1000 }, (err, signedToken) => {

                                    if (err) {
                                        console.log("error in generating token", err)
                                    }
                                    else {
                                        res.json({ message: "user loggedin successfully", usertoken: signedToken, username: userloginResult.username })
                                    }
                                })


                            }


                        })

                    }

                    else {
                        res.json({ message: "wrong credentials" })
                    }

                })
        }
    })
})

//requests from user Module
//Requests from profile component (user Module)

app.get("/getStudentDetailsById/:stdId", (req, res) => {

    studentCollection.findOne({ stdId: req.params.stdId }, (err, stdObj) => {
        if (err) {
            console.log("error in find student details", err)
        }
        else if (stdObj !== null) {
            res.json({ message: stdObj })
        }
        else {
            res.json({ message: "student data not found" })
        }
    })
})

//req from sattendance component
//get attendance data of student by stdId

app.get("/getStdAtten/:batchname", (req, res) => {


    // console.log("now", req.params.batchname, req.params.stdId)
    attendanceCollection.find({ batch: req.params.batchname }).toArray((err, attenArray) => {

        if (err) {
            console.log("error in reading attendance data", err)
        }
        else if (attenArray.length == 0) {

            res.json({ message: "attendance data not found" })
        }
        else {

            res.json({ message: attenArray })
        }
    })
})


//user settings component reqs
//update password req handler
app.put("/changeCredentials/:stdId", (req, res) => {

    userloginCollection.findOne({ username: req.params.stdId }, (err, userObj) => {

        if (err) {
            console.log("error in finding user", err);

        }
        else if (userObj !== null) {
            bcrypt.hash(req.body.Cpassword, 8, (err, HashedPass) => {

                if (err) {
                    console.log("error in hasing password");

                }
                else {

                    console.log(HashedPass)
                    userloginCollection.updateOne({ username: req.params.stdId }, { $set: { password: HashedPass } }, (err, updatePass) => {

                        if (err) {

                            console.log("error in updating password");

                        }
                        else {
                            res.json({ message: "password Updated successfully" })
                        }

                    })

                }
            })
        }
    })

})

// //forgot password req handler
// app.post("/forgetPassword", (req, res) => {
//     // console.log(req.body.Fusername);

//     userloginCollection.findOne({ username: req.body.Fusername }, (err, StdObj) => {



//         if (err) {
//             console.log("error in finding data", err);

//         }
//         else if (StdObj !== null) {

//             console.log(StdObj.username);
//             studentCollection.findOne({ stdId: StdObj.username }, (err, oldStdObj) => {

//                 if (err) {
//                     console.log("error in finding data", err);

//                 }
//                 else if (oldStdObj !== null) {

//                     // console.log(oldStdObj.name);

//                     //send otp to user

//                     let otp = Math.floor(Math.random() * 9999) + 1111;

//                     // console.log(otp);

//                     var otptime = new Date().getTime() + (60 * 1000);


//                     //add otp and otptime to req.body
//                     req.body.otp = otp;
//                     req.body.timestamp = otptime;


//                     // console.log(req.body);
//                     otpCollection.insertOne(req.body, (err, result) => {
//                         if (err) {
//                             console.log("error in inserting otp", err);

//                         }
//                         else {

//                             //send otp to mail

//                             var mailOptions = {

//                                 from: 'murariadurthi.5859@gmail.com',
//                                 to: oldStdObj.email,
//                                 subject: 'Ims One time Password ',
//                                 text: 'hi your one time password is',
//                                 html: `<p>otp:${req.body.otp}</p>`
//                             }
//                             tranporter.sendMail(mailOptions, (err, info) => {
//                                 if (err) {

//                                     console.log("error in sending mail", err)
//                                 }
//                                 else {

//                                     res.json({ message: 'mail sent', stdid: StdObj.username });

//                                 }

//                             })

//                         }
//                     })

//                 }
//                 else {

//                     res.json({ message: "user data not found" })
//                 }
//             })


//         }
//         else {

//             res.json({ message: "user not found on this username" })
//         }
//     })


// })

//verify otp req handler
app.post("/verifyOtp/:stdId", (req, res) => {

    otpCollection.findOne({ Fusername: req.params.stdId, otp: +req.body.otp }, (err, otpresult) => {
        if (err) {
            console.log("error in finding otp");

        }
        else if (otpresult !== null) {
            var otptime = new Date().getTime()
            console.log(otptime);

            if (otptime < otpresult.timestamp) {


                res.json({ message: "otp verified" })

                otpCollection.deleteOne({ Fusername: req.params.stdId }, (err, deleteOtp) => {

                    if (err) {
                        console.log("error in delting otp", err);

                    }
                    else {
                        console.log("otp delted successfully");

                    }
                })

            }
            else {

                res.json({ message: "otp expired" })

                otpCollection.deleteOne({ Fusername: req.params.stdId }, (err, deleteOtp) => {

                    if (err) {
                        console.log("error in delting otp", err);

                    }
                    else {
                        console.log("otp delted successfully");

                    }
                })


            }
        }
        else {
            res.json({ message: "wrong Otp" })
        }
    })

})

// app.get('/*', (req, res) => {

//     res.sendFile(path.join(__dirname + './dist/ims-app'))
// })

//Assign port for server
const PORT = process.env.PORT || 3000

app.listen(PORT, () => console.log(`server is running on ${PORT}`))



