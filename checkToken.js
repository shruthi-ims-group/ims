//import jsonwebtoken
const jwt = require("jsonwebtoken")

checkAuth = (req, res, next) => {

    // console.log("token header", req.headers["authorization"]);

    var token = req.headers["authorization"]

    if (token == undefined) {


        return res.json({ message: "no token found" })
    }
    if (token.startsWith("Bearer ")) {

        //remove Bearer from string
        token = token.slice(7, token.length)
        jwt.verify(token, "secret", (err, decoded) => {

            if (err) {
                return res.json({ message: "session expired" })
            }
            next()
        })
    }
}

module.exports = checkAuth;